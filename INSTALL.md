# Construct project
***

## By spack

First, you need to install [spack](https://github.com/spack/spack) on your labtop.

### user mode

Insert the [spack recipe](./spack_recipe/package.py) in your spack installation, then :
<pre>
 $ spack install time-angle ${spack_options}
</pre>

### developer mode

<pre>
 $ git clone https://gitlab.com/racinej/time_angle.git ; cd time_angle
 $ spack dev-build time-angle@0.0.0 ${spack_options}
</pre>

## By cmake

### Take the project.
<pre>
 $ git clone https://gitlab.com/racinej/time_angle.git ; cd time_angle
</pre>

### Put correct environment.
<pre>
 $ module load cmake          # to configure 
 $ module load py-sphinx      # to make doc 
 $ module load doxygen        # to make doc 
 $ module load googletest     # Regression testing 
 $ module load f2py/python    # python binding
</pre>

### Building binaries.
<pre>
 $ mkdir build ; cd build
 $ cmake .. ${cmake\_options}
 $ make
 $ make install
 $ make test
</pre>


### Options
<pre>

              CMAKE                                       Spack
    -DCMAKE_BUILD_TYPE=                                   
                Release                                    build_type=Release
                Debug                                      build_type=Debug
                RelwithDebinfo (default)                   None
                MinSizeRel                                 build_type=MinSizeRel

    -DENABLE_COVERAGE=                                  
                OFF (default)                              None
                ON  (only with Debug mode)                 +cov

    -DTA_ENABLE_SHARED=      
                OFF (default)                              None
                ON                                         +shared

    -DTA_ENABLE_TESTS=
                OFF (default)                              None
                ON                                         +tests

    -DTA_ENABLE_DOC=  
                OFF (default)                              None
                ON                                         +doc

    -DTA_ENABLE_PY=   
                OFF (default)                              None
                ON                                         +py 
</pre>
