program cov_time_angle
!
use mod_time_angle
!
implicit none
!
integer, parameter :: n_heur = 2
integer            :: ih(n_heur), im(n_heur)
real               :: angle(n_heur)
integer            :: i, ierr
!
!
ih(1)  = 13
ih(2)  = -14
!
im(1)  =  0
im(2)  =  0
!
print*, '---------------------------------'
print*, '-- hour -- min ----- angle ------'
print*, '---------------------------------'
do i=1, n_heur
   call time_angle(hour=ih(i),         &
                   minute=im(i),       &
                   angle=angle(i),     &
                   ierr=ierr)
   write(6,'(2(5x,I2), 7x F5.1)') ih(i), im(i), angle(i)
enddo
print*, '---------------------------------'
!
end program
