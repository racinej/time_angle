#include <iostream>

#include "mod_time_angle.h"

int main(int argc, char *argv[])
{
	int   hour=15;
	int   minute=15;
	float angle;
 	int   ierr=0;

	angle=time_angle(hour,minute);

//        if (ierr != 0)
//	{
//		return 1;
//	}
	std::cout << "at " << hour  <<":"<< minute << ", the angle is "  << angle  << std::endl;

	return 0;
}
