# Coverage 
***

There are 3 executables to test the coverage of *Time Angle*: Fortran, C and error coverage.

> $   mkdir build ; cd build  
> $   cmake .. -DCMAKE_BUILD_TYPE=Debug -DENABLE_COVERAGE=ON  
> $   make cov_time_angleF  
> $   make cov_time_angleF_err1  
> $   make cov_time_angleC  
> $   ./cov/cov_time_angleF  
> $   ./cov/cov_time_angleF_err1  
> $   ./cov/cov_time_angleC  
> $   make time_angle_static-genhtml  
> $   firefox lcov/html/time_angle_static.html

