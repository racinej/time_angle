!> ta_delt_abs_hm
!! Get number without referential
!!
subroutine ta_delt_abs_hm(mod_ta,a1,a2,delta)
!
use mod_angle_time
!
implicit none
!
real   , intent(in)     :: a1      !< hour
real   , intent(in)     :: a2      !< minute
real   , intent(out)    :: delta   !< absolute difference of 2 hands
type (typ_ang_tim)      :: mod_ta
!
delta = 0.
!
delta = abs(a2 - a1)
!
if (delta .lt. 0.) mod_ta%ierr = 1
if (mod_ta%verb .eq. 2 ) write(6,mod_ta%verb_2_r) "TIME_ANGLE> delta = ", delta
!
return
!
end subroutine
