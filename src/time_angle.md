DOCUMENTATION DEVELOPPEUR {#mainpage}
=========================

Documentation développeur de la bibliothèque **TIME\_ANGLE**.

- [Documentation développeur](../developer/index.html) \n
- [Manuel théorique](../theoric/index.html)            \n
- [Manuel utilisateur](../user/index.html)             \n
\n
\n

Les outils et les langages :
----------------------------

|  OUTILS                                                                | LANGAGES                                                                                    |
| :--------------------------------------------------------------------- | :------------------------------------------------------------------------------------------ |
| [gcc : GNU compiler collection](https://gcc.gnu.org/)                  | [html : Hypertext Markup Language](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language) |
| [cmake](https://cmake.org/)                                            | [Tcl : Tool Command Language](https://fr.wikipedia.org/wiki/Tool_Command_Language)          |
| [sphinx](https://www.sphinx-doc.org/en/master/index.html)              | [Fortran : Formula translator](https://www.fortran.com/)                                    |
| [module](https://en.wikipedia.org/wiki/Environment_Modules_(software)) | [RST : ReStructuredText](https://fr.wikipedia.org/wiki/ReStructuredText)                    |
| [latex](https://www.latex-project.org/)                                |                                                                                             |
| [ninja](https://ninja-build.org/)                                      |                                                                                             |

\n
\n

Inspiration :
-------------

- [youtube : 07 avril 2021](https://www.youtube.com/watch?v=FGyh7BmIzrM) \n


Julien RACINE
