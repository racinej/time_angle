!> ta_check_input
!! check reasonnable integer for hour
!! and reasonna ble integer for min
!!
subroutine ta_check_input(mod_ta,h,m)
!
use mod_angle_time
!
implicit none
!
integer, intent(in)     :: h       !< hour
integer, intent(in)     :: m       !< minute
type (typ_ang_tim)      :: mod_ta
!
! hour
! TODO select type (h)
! TODO type  is (real(kind=*))
! TODO   goto 1000
! TODO class is (character(kind=*))
! TODO   goto 1000
! TODO class is (logical)
! TODO   goto 1000
! TODO end select
!
if (h <  0) goto 1001
if (h > 24) goto 1002
!
! min
! TODO select type (m)
! TODO class is (real(kind=*))
! TODO   goto 1010
! TODO class is (character(kind=*))
! TODO   goto 1010
! TODO class is (logical)
! TODO   goto 1010
! TODO end select
!
if (m <  0) goto 1011
if (m > 60) goto 1012
!
if (mod_ta%verb .eq. 2 ) write(6,mod_ta%verb_2_i) "TIME_ANGLE> h = ", h
if (mod_ta%verb .eq. 2 ) write(6,mod_ta%verb_2_i) "TIME_ANGLE> m = ", m
!
return
!
! TODO 1000  write(6,*) "hour should be integer."
! TODO       mod_ta%ierr = 1
1001  write(6,*) "hour number negative ! it is impossible."
      mod_ta%ierr = 1
1002  write(6,*) "hour number > 24     ! it is impossible."
      mod_ta%ierr = 1
!
! TODO 1010  write(6,*) "min  should be integer."
! TODO       mod_ta%ierr = 1
1011  write(6,*) "min  number negative ! it is impossible."
      mod_ta%ierr = 1
1012  write(6,*) "min  number > 60     ! it is impossible."
      mod_ta%ierr = 1
!
end subroutine
