

// Fortran function
extern "C" void time_angle_(int *hour, int *minute, float *angle, int *ierr, int *verbosity);

// C function return angle
float time_angle(int hour, int minute) // int ierr, ...)
{
// optional verb	va_list args;
// optional verb	va_start(args, ierr);
// optional verb	int verb = va_arg(args,int);
// optional verb	va_end(args);

       if ((hour   > 24)||(hour  <0))
       {
	       return 1;
       }
       if ((minute > 60)||(minute<0))
       {
	       return 1;
       }

       float angle;
       int ierr=0;
       int verbosity=0;
       // calling Fortran
       time_angle_(    &hour,     &minute,       &angle,     &ierr, &verbosity);
       return angle;
}
