# The Angle of Time !
***

This is a simple library to know the angle between hour hand and minute hand of the clock.


## User

See user doc to know how call this library.
You can get it by some tools : spack, cmake, python script, C, C++ or Fortran program.


## Developer

### Can I construct this project ?

see [INSTALL](./INSTALL.md) file.


### Can I run some Tests ?

<pre>
  # need google test for this numerical test  
  $ make test  
  $ ./test/nr/nr
</pre>

### Is There correct Coverage ?

see [coverage README](./cov/README.md) file.

### Can I see the Documentation ?

There are 3 html documentations, generate by sphinx (user+theoric) and generate by doxygen (developer) :

<pre>
$ make doc_developer ; firefox .../share/doc/developer/index.html  
$ make doc_theoric   ; firefox .../share/doc/theoric/index.html  
$ make doc_user      ; firefox .../share/doc/user/index.html
</pre>
