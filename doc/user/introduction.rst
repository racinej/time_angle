
Introduction
============

Ce document constitue le manuel utilisateur de la biliothèque **TIME_ANGLE**.
Il s'agit d'une bibliothèque de calcul scientifique en interface Fortran/C se plaçant dans une géométrie à 2 dimensions de type "cadran d'horloge".


.. include:: ./graph_global.rst

L'utilisateur doit écrire un programe Fortran ou C.
Il doit ensuite appeler cette bibliothèque avec trois arguments, l'heure, la minute et l'angle.
Enfin, la valeur de l'angle formé par l'aiguille des heures et l'aiguille des minutes lui est rendue par **TIME_ANGLE**.
Pour voir le calcul fait : cf Détails--> Manuel théorique.

