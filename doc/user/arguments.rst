
Arguments d'entree
==================

La bibliothèque **TIME_ANGLE** accepte des arguments obligatoires et optionnels :
Parmis les arguments obligatoires :

1. *hour*

Il s'agit du premier argument d'appel à la bibliothèque. 
Il doit s'agit d'un entier entre 0 et 24 (inclus).

2. *min* 

Il s'agit du second argument d'appel à la bibliothèque. 
Il doit s'agit d'un entier entre 0 et 60 (inclus).

3. *angle*

Il s'agit du troisième argument d'appel à la bibliothèque. 
Il s'agit d'un réel positif entre 0 et 180 (inclus) calculé par la biliothèque. 

Parmis les arguments optionnel :

3. *ierr*

Flag d'erreur, il s'agit d'un entier.
Il indique si le calcul s'est bien passé (ierr=0) ou si la bibliothèque a rencontré un problème (ierr différent de 0).

4. *verb* 

Verbosité du calcul, il s'agit d'un entier.
Par défaut, la verbosité est nulle (verb=0), la bibliothèque ne renvoit aucun message.
Les valeurs possible sont : 0,1,2.

