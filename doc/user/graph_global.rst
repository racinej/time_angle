
.. tikz:: 

   \filldraw [fill=white] (0,0) circle [radius=2cm];
   \foreach \angle [count=\xi] in {60,30,...,-270}
   {
     \draw[line width=1pt] (\angle:1.9cm) -- (\angle:2cm);
     \node[font=\large] at (\angle:1.60cm) {\textsf{\xi}};
   }
   \draw[line width=1pt] (0,0) -- (-60:0.8cm);
   \draw[line width=1pt] (0,0) -- (90:1.2cm);


