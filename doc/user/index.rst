##################
MANUEL UTILISATEUR
##################



.. include:: ./graph_global.rst


.. toctree::
   :maxdepth: 2
   :numbered:

   ./introduction.rst
   ./arguments.rst
   ./ex_appel.rst
   ./conclusion.rst



