
Exemples d'appel 
================

La bibliothèque est accessible par cmake via la variable d'environnement "time_angle_DIR" :

.. code-block:: cmake
   :linenos:

   find_package(time_angle)
   if(time_angle_FOUND)
     message(STATUS "TA VERSION : ${time_angle_VERSION}")
     message(STATUS "TA HEADERS : ${time_angle_INCLUDES}")
     message(STATUS "TA LIBRARY : ${time_angle_LIBRARIES}")
   else()
     message(FATAL_ERROR "time_angle not found")
   endif()

   add_executable(EX ...)
   target_link_libraries(EX               ${time_angle_LIBRARIES})
   target_include_directories(toto PUBLIC ${time_angle_INCLUDES})


La bibliothèque est accessible par spack :

.. code-block:: bash
   :linenos:

   git clone ...
   # see recipe here
   cat time_angle/spack-recipe/package.py
   # copy it in your spack
   spack install time_angle
   spack load time_angle


L'appel Fortran se fait de la façon suivante (l'exemple pris n'utilise que les trois arguments obligatoires) :

.. code-block:: fortran
   :linenos:

   use mod_time_angle !< check interface
   !
   integer :: my_hour, my_minute
   real    :: my_angle
   !
   my_hour   = 15
   my_minute = 15
   call time_angle (hour=my_hour,     &
                    minute=my_minute, &
                    angle=my_angle)
   print*, " Angle is : ", my_angle

Dans cet exemple, le test se fait pour l'heure de :math:`\color{red}{15}` h :math:`\color{blue}{15}` min.
L'angle entre les deux aiguilles est de :math:`7.5` degrés.

L'appel C se fait de la façon suivante, l'exemple pris n'utilise que les trois arguments obligatoires et est similaire à l'exemple Fortran pris ci-dessus:

.. code-block:: cpp
   :linenos:

   #include "mod_time_angle.h"
   
   int my_hour 15;
   int my_minute 15;
   float my_angle = time_angle(my_hour,my_minute);
   std::cout << " Angle is : "<< my_angle   << std::endl;


Le module python de cette bibliothèque est également disponible.
L'appel se fait de la manière suivante : 

.. code-block:: python
   :linenos:

   import time_angle as ta

   angle = 0.0
   ta.time_angle(3,24,angle)

   print("At 3:24 the angle of time is : ", angle)


