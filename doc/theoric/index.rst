################
MANUEL THEORIQUE  
################



.. include:: ./../user/graph_global.rst



.. toctree::
   :maxdepth: 2
   :numbered:

   ./introduction.rst
   ./check_hm.rst
   ./calcul.rst






