
Vérification des heures et des minutes 
======================================

D'abord, une routine est chargée de vérifier les deux arguments d'entrée obligatoires, l'entier correspondant aux heures et celui correspondant au minute.
Soit :math:`\color{purple}{h}` le premier argument transmis et :math:`\color{purple}{m}` le second, voici les vérifications de cette routine.


.. math::     h &\, entier\, positif   \\[-0.4cm]
              h &>= 0             \\[-0.4cm]
              h &<= 24            \\[-0.4cm]

.. math::     m &\, entier\, positif   \\[-0.4cm]
              m &>= 0             \\[-0.4cm]
              m &<= 60            \\[-0.4cm]


