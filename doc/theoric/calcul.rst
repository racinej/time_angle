
Calcul
======

Le choix a été fait que l'aiguille des minutes représente une référence.
Soit :math:`\color{red}{h}` le premier argument fourni et :math:`\color{blue}{m}` le second.
On se place d'abord dans un 'référentiel de la minute' :

.. math::    cte1 &= 5            \\
             a1   &= h * cte1     \\
             a2   &= m

Soit :math:`\color{red}{a1}` la transformation de :math:`\color{red}{h}` et :math:`\color{blue}{a2}` la transformation de :math:`\color{blue}{m}`.
La constante correspond au fait que l'aiguille des heures parcours une unité alors que l'aiguille des minutes fait ce parcours en 5 unité.


Avance de l'aiguille des heures
-------------------------------

Il est courant de penser que l'aiguille des heures avance grace à l'avance de l'aiguille des minutes.
Ainsi le terme :math:`\color{red}{a1}` doit être corrigé (par le terme :math:`\epsilon`) en fonction de la position de l'aiguille des minutes.

.. math::    cte2     &= \frac{1}{12}  \\
                      &= \frac{5}{60}  \\
             \epsilon &= cte2 * a2     \\
             a1       &= a1 + \epsilon

La constante utilisé ici correspond au fait que l'aiguille des heures parcourt l'équivalent de '5 min' (1 unité d'heure) lorsque l'aiguille des minutes parcourt 60 unités.
 
  
Des minutes en dehors de l'horloge
----------------------------------

On se place maintenant dans un système dépourvu de réferentiel.
Soit :math:`\Delta` la différence des deux entités transformées :math:`\color{red}{a1}` et :math:`\color{blue}{a2}`. 

.. math::    \Delta = |a2 - a1| 


Passage des minutes aux degrés
------------------------------

Le résultat obtenu de :math:`\Delta` est dans une unité similaire à la minute.
Il faut donc convertir ce résultat en degrés d'angle au moyen d'une constante, soit :math:`\theta` le résultat obtenu.

.. math::    cte3       &= 6                             \\
                        &= \frac{360}{60}                \\
             \theta     &= \Delta * cte3                 \\[0.4cm]

Enfin, une série de vérification est faite sur le terme :math:`\theta` (positivité et angle saillant) pour renvoyer l'angle final au programme.

.. math::    \alpha      &= \theta                \\
            avec\,\alpha &>= 0                    \\
                  et\,   &<= 180                     

