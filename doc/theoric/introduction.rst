
Introduction
============

Ce document constitue le manuel théorique de la bibliothèque **TIME_ANGLE**.
Il s'agit d'une API de calcul scientifique en interface Fortran/C se plaçant dans une géométrie à 2 dimensions de type "cadran d'horloge".

.. include:: ./../user/graph_global.rst


Ce document précise comment la bibliothèque calcule l'angle entre deux aiguilles d'une horloge.



