from spack import *

class TimeAngle(CMakePackage):
    """The Angle of time !"""

    homepage = "https://gitlab.com/racinej/time_angle/"
    url      = "https://gitlab.com/racinej/time_angle/-/archive/0.0.1/time_angle-0.0.1.tar.gz"

    maintainers = ['racinej']

    version('0.0.7', sha256='925642825dd773a2e52104d335e7d7427259cc9563760098b639c922f8a69c15')
    version('0.0.6', sha256='5f266bcf56b6bf1472c59d33eb6ad4bc3084260b47fb4d3f5657c8517c96b768')
    version('0.0.5', sha256='02077cf948697f62f440cd75bfddaae62ed54fcef30f1a6847737452aea2f974')
    version('0.0.4', sha256='b4f5e369a027c5daa59967abcfee65e22622ae68eca21c515941aba26b8b76d6')
    version('0.0.3', sha256='7bceb5aebed9e55c90fff7313d3136a578a1e276cf1852097b70983ffcab2c56')
    version('0.0.2', sha256='f98d5144adda2637776edf6fe8f6f7bbbd3fabca9d6d2238917e42b32b424394')
    version('0.0.1', sha256='69bc458f6a3dde8bf42cef71b1b12d1e0ba6a594a078d03a1531a34aca640c1e')

    variant('tests',  default=False, description='install unitary + NR tests')
    variant('doc',    default=False, description='install theoric+user+developer docs')
    variant('shared', default=False, description='use shared library')
    #variant('py',     default=False, description='construct binding python')

    depends_on('googletest',  type=('build','run'),   when="+tests")

    depends_on('doxygen',     type="build",           when="+doc")
    depends_on('py-sphinx',   type="build",           when="+doc")

    #depends_on('python',       type=('build','run'),   when="+py")
    #depends_on('py-numpy',     type=('build'),         when="+py")

    # creation d une variable pour faire :
    # spack install time-angle +docs
    # firefox time-angle_doc
    def cmake_args(self):
        args = []
        args.append('-DTA_ENABLE_SHARED=ON')
        if '+tests'   in self.spec:
            args.append('-DTA_ENABLE_TESTS=ON')
        if '+doc'     in self.spec:
            args.append('-DTA_ENABLE_DOC=ON')
        if '+shared'  in self.spec:
            args.append('-DTA_ENABLE_SHARED=ON')
        #if '+py'      in self.spec:
        #    args.append('-DTA_ENABLE_PY=ON')

        return args
