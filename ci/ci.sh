#!/bin/bash

# linux : ubuntu, debian, centos-8, openSuse

list_os="linux" #windows mac"                 # operating system          linux, windows, mac
list_c="gcc"                                # compilo                   clang, pgi, gnu, intel
list_t="Debug RelWithDebInfo"               # build type                rel deb relwithdebinfo
#list_v="shared doc tests"                   # variant                   shared doc tests


echo ""
echo "stages:"
echo "  - initialize"
echo "  - build"
echo "  - test"
echo "  - deploy"
echo "  - finalize"
echo ""


# -------------------------------------------------------------------
# stage = initialize
# -------------------------------------------------------------------
stage="initialize"
echo "debut:"
echo "    stage: ${stage}"
echo "    script:"
echo "      - git clone https://github.com/spack/spack.git my_spack"
echo '      - export PATH="${PATH}:${CI_PROJECT_DIR}/my_spack/bin"'
echo "      - spack create  --skip-editor https://gitlab.com/racinej/time_angle"
echo '      - cp ${CI_PROJECT_DIR}/spack_recipe/package.py my_spack/var/spack/repos/builtin/packages/time-angle/'
echo ""
echo "    artifacts:"
echo "         paths:"
echo "           - \${CI_PROJECT_DIR}/my_spack"
echo "         expire_in: 1 day"
echo ""



# -------------------------------------------------------------------
# stage = build
# -------------------------------------------------------------------
stage="build"
for os in ${list_os}
do
  for c in ${list_c}
  do
    for t in ${list_t}
    do
        echo "${os}_${c}_${t}:${stage}:"
        echo "    stage: ${stage}"
        #echo "    tags: [gitlab-org]"
        echo "    needs: [\"debut\"]"

	echo ""
        echo "    before_script:"
        echo '      #- export PATH="${PATH}:${CI_PROJECT_DIR}/my_spack/bin"'
        if [ "${os}" == "linux" -o "${os}" == "mac" ]
        then
          echo "      - export DEBIAN_FRONTEND=noninteractive"
          echo "      - apt-get update"
          echo "      - apt-get install -y gfortran g++ git curl python3"
	  echo "      - apt-get install -y cmake libgtest-dev doxygen"
        fi
	if [ "${os}" == "windows" ]
	then
	  echo "      - start powershell"
	  echo "      - pip install gfortran g++ git curl python3"
        fi
        
        echo ""
	echo "      # on one proc, too long... spack dev-build -u build -q time-angle@0.0.0%${c} +shared +doc +tests build_type=${t} ^cmake%gcc "
        echo "    script:"
	echo "      - mkdir cmake-build ; cd cmake-build "
	echo "      - cmake .. -DCMAKE_BUILD_TYPE=${t} -DTA_ENABLE_SHARED=ON -DTA_ENABLE_TESTS=ON -DTA_ENABLE_DOC=OFF" 
        echo "      - make "

        echo ""
      	echo "    artifacts:"
	echo "         paths:"
	echo "           - \${CI_PROJECT_DIR}/cmake-build"
        echo "         expire_in: 1 day"
        echo ""
    done
  done
done


# -------------------------------------------------------------------
# stage = test
# -------------------------------------------------------------------
stage="test"
for os in ${list_os}
do
  for c in ${list_c}
  do
    for t in ${list_t}
    do
        echo "${os}_${c}_${t}:${stage}:"
        echo "    stage: ${stage}"
        #echo "    tags: [gitlag-org]"
        echo "    needs: [\"${os}_${c}_${t}:build\",\"debut\"]"
        echo ""

        echo "    before_script:"
        if [ "${os}" == "linux" -o "${os}" == "mac" ]
        then
          echo "      - export DEBIAN_FRONTEND=noninteractive"
          echo "      - apt-get update"
          echo "      - apt-get install -y gfortran g++ git curl python3"
	        echo "      - apt-get install -y cmake libgtest-dev doxygen"
        fi
	      if [ "${os}" == "windows" ]
	      then
	        echo "      - start powershell"
	        echo "      - pip install gfortran g++ git curl python3"
        fi

        echo ""
        echo "    script:"
        echo "        - cd \${CI_PROJECT_DIR}/cmake-build"
        echo "        - make test"
        echo "        - ./test/nr/ta_nr"
        echo "        - ./test/unit/ta_c_calling"
        echo "        - ./test/unit/ta_t1_morning"
        echo "        - ./test/unit/ta_t2_lunch"
        echo "        - ./test/unit/ta_t3_afternoon"
        echo "        - ./test/unit/ta_t4_evening"
        echo "        - ./test/unit/ta_t5_night"
        echo "        - ./test/unit/ta_t6_midnight"

        echo ""
    done
  done
done

# -------------------------------------------------------------------
# stage = deploy
# -------------------------------------------------------------------
stage="deploy"
for os in ${list_os}
do
  for c in ${list_c}
  do
    for t in ${list_t}
    do
        echo "${os}_${c}_${t}:${stage}:"
        echo "    stage: ${stage}"
        #echo "    tags: [gitlab-org]"
        echo "    needs: [\"${os}_${c}_${t}:build\",\"debut\"]"

        echo ""
        echo "    before_script:"

        echo ""
        echo "    script:"
	echo '        - echo "no deploy without spack" '
        echo "        #- spack install time-angle%${c} +doc +tests +shared build_type=${t} ^cmake%gcc "

        echo ""
    done
  done
done


# -------------------------------------------------------------------
# stage = finalize
# -------------------------------------------------------------------
stage="finalize"
echo "fin:"
echo "    stage: ${stage}"
echo "    needs:"
for os in ${list_os}
do
  for c in ${list_c}
  do
    for t in ${list_t}
    do
      echo "        - job: ${os}_${c}_${t}:test "
    done
  done
done

echo ""
echo "    script:"
for os in ${list_os}
do
  for c in ${list_c}
  do
    echo "        - echo \"machine -- ${os} OK \" "
    echo "        - echo \"comp    -- ${c} OK \" "
  done
done
echo ""

